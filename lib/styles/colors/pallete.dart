import 'package:flutter/material.dart';

class PaletteSec {
  static const MaterialColor SecondaryColor = const MaterialColor(
    0xff2596be, // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
    const <int, Color>{
      50: const Color(0xff134b5f), //50%
      60: const Color(0xff0f3c4c), //60%
      30: const Color(0xff1a6985), //30%
      100: const Color(0xff000000), //100%
    },
  );
}

class Palette {
  static const MaterialColor PrimaryColor = const MaterialColor(
    0xfff0c020, // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
    const <int, Color>{
      70: const Color(0xff604d0d), //70%
      100: const Color(0xff0000000), //100%
    },
  );
}
