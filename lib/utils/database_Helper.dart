import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart' as sql;

class SQLHelper {
  static Future<void> createTables(sql.Database database) async {
    await database.execute("CREATE TABLE itemName ("
        "id INTEGER PRIMARY KEY AUTOINCREMENT,"
        "title TEXT,"
        "createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"
        ")");
    await database.execute("CREATE TABLE item ("
        "id INTEGER PRIMARY KEY AUTOINCREMENT,"
        "item_description TEXT,"
        "itemNameFk INTEGER, FOREIGN KEY (itemNameFk) REFERENCES itemName (id)"
        ")");
  }
// id: the id of a item
// title, description: name and description of your activity
// created_at: the time that the item was created. It will be automatically handled by SQLite

  static Future<sql.Database> db() async {
    return sql.openDatabase(
      'groceryList.db',
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
      },
    );
  }

  // Create new itemName
  static Future<int> createItemName(String title) async {
    final db = await SQLHelper.db();

    final data = {'title': title};
    final id = await db.insert('itemName', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

// Create new items
  static Future<int> createItems(String itemDesc, var fk) async {
    final db = await SQLHelper.db();

    final data = {'item_description': itemDesc, 'itemNameFk': fk};
    final id = await db.insert('item', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  // Read all itemNames (journals)
  static Future<List<Map<String, dynamic>>> getAllItemNames() async {
    final db = await SQLHelper.db();
    return db.query('itemName', orderBy: "id");
  }

  // Read a single itemName by id

  static Future<List<Map<String, dynamic>>> getItemName(var id) async {
    final db = await SQLHelper.db();
    return db.query('itemName', where: "id = ?", whereArgs: [id]);
  }
  // Read a single item by FKid

  static Future<List<Map<String, dynamic>>> getItemByFk(var id) async {
    final db = await SQLHelper.db();
    return db.query('item', where: "itemNameFk = ?", whereArgs: [id]);
  }

  // Updating ItemName
  static Future<int> updateItem(int id, String title) async {
    final db = await SQLHelper.db();

    final data = {'title': title, 'createdAt': DateTime.now().toString()};

    final result =
        await db.update('itemName', data, where: "id = ?", whereArgs: [id]);
    return result;
  }

  // Deleting itemTitle
  static Future<int> deleteItemName(int id) async {
    final db = await SQLHelper.db();
    int result = 0;
    try {
      result = await db.delete("itemName", where: "id = ?", whereArgs: [id]);
    } catch (err) {
      debugPrint("Something went wrong when deleting an item: $err");
    }
    return result;
  }

  //Deleting itens with the itenTitle ID
  static Future<int> deleteItems(int id) async {
    final db = await SQLHelper.db();
    int result = 0;
    try {
      result =
          await db.delete("item", where: "itemNameFk = ?", whereArgs: [id]);
    } catch (err) {
      debugPrint("Something went wrong when deleting an item: $err");
    }
    return result;
  }
}
