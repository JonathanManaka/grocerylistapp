import 'package:flutter/material.dart';
import 'package:mygrocerylist/views/ResponsiveWidgets/addItemsMaterialUI.dart';

class CreateGlistScreen extends StatefulWidget {
  const CreateGlistScreen({Key? key, required this.groceryListName})
      : super(key: key);
  final String groceryListName;

  @override
  State<CreateGlistScreen> createState() => _CreateGlistScreenState();
}

class _CreateGlistScreenState extends State<CreateGlistScreen> {
  final _formKey = GlobalKey<FormState>();

  List<String> items = [];
  String title = 'Items';
  int maxheight = 0;
  TextEditingController newItemTextCront = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    items.clear();
  }

  @override
  Widget build(BuildContext context) {
    //Responsive design
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      if (constraints.maxHeight >= 1440) {
        return addItemsMaterialUIWidget(
            maxHeight: 500, groceryListName: widget.groceryListName);
      } else {
        return addItemsMaterialUIWidget(
            maxHeight: 450, groceryListName: widget.groceryListName);
      }
    });
  }
}
