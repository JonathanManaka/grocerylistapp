import 'package:flutter/material.dart';
import 'package:mygrocerylist/utils/database_Helper.dart';
import 'package:go_router/go_router.dart';
import 'package:mygrocerylist/views/home_Screen.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mygrocerylist/styles/colors/pallete.dart';

class StartShoppingScreen extends StatefulWidget {
  const StartShoppingScreen({Key? key, required this.itemNameid})
      : super(key: key);
  final int itemNameid;
  @override
  State<StartShoppingScreen> createState() => _StartShoppingScreenState();
}

class _StartShoppingScreenState extends State<StartShoppingScreen> {
  List<Map<String, dynamic>> _ItemsJournal = [];
  List<Map<String, dynamic>> _itemName = [];
  bool _isLoading = true;
  String itemNameAppBar = "";
  TextEditingController budgetTextContr = TextEditingController();
  TextEditingController ItemCalTextContr = TextEditingController();
  double budget = 0.0;
  double groceryTot = 0.0;
  double newBudget = 0.0;
  double budgetLeft = 0.0;
  //Default message on the bottom appBar
  String budgetText = "Click top left icon,";
  String groceryTotText = "To create your bubget";
  //List of item prices. They will be added from the openDialog
  List<double> itemPrices = [];
  bool ischeckedValue = false;
  int itemsLength = 0;

  //This method fetches all data from the database
  void _refreshItemJournal() async {
    final data = await SQLHelper.getItemByFk(widget.itemNameid);
    setState(() {
      _ItemsJournal = data;
      itemsLength = _ItemsJournal.length;
      //This function adds default item prices with the length of the itemsJournal
      for (int i = 0; i < itemsLength; i++) {
        itemPrices.add(0.0);
      }
      _isLoading = false;
    });
  }

  void _refreshItemName() async {
    final data = await SQLHelper.getItemName(widget.itemNameid);
    setState(() {
      _itemName = data;
      itemNameAppBar = _itemName[0]['title'];
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _refreshItemJournal();
    _refreshItemName();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(
          seedColor: Palette.PrimaryColor,
          brightness: Brightness.light,
        ),
      ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Palette.PrimaryColor,
          leading: IconButton(
            onPressed: () {
              openBudgetDialog();
            },
            icon: Icon(Icons.money_rounded),
            iconSize: 30,
            color: Palette.PrimaryColor[100],
          ),
          title: Text('${itemNameAppBar.toUpperCase()}',
              style: GoogleFonts.lato(
                  fontSize: 15,
                  fontWeight: FontWeight.w800,
                  color: Palette.PrimaryColor[100])),
          centerTitle: true,
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: IconButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => MyHomePage()));
                },
                icon: Icon(Icons.home),
                iconSize: 30,
                color: Palette.PrimaryColor[100],
              ),
            ),
          ],
        ),
        body: _isLoading
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          height: 380,
                          child: ListView.builder(
                            itemCount: _ItemsJournal.length,
                            itemBuilder: (BuildContext context, int index) {
                              return buldItem(
                                item: Text(
                                  _ItemsJournal[index]['item_description'],
                                  style: GoogleFonts.lato(
                                    fontSize: 17,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                iconBtnSzb: SizedBox(
                                  width: 80,
                                  child: Row(children: <Widget>[
                                    Text(
                                      '${addMillionSufx(itemPrices[index])}',
                                      style: GoogleFonts.lato(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ]),
                                ),
                                onClicked: () {
                                  setState(() {
                                    openItemCalcutDialog(index);
                                  });
                                },
                              );
                            },
                          )),
                    ]),
              ),
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(color: Colors.green, width: 2)),
                  child: Padding(
                    padding: EdgeInsets.all(6.0),
                    child: Text(
                      'Budget:   $newBudget',
                      style: GoogleFonts.lato(
                          color: Palette.PrimaryColor[100],
                          fontSize: 11,
                          fontWeight: FontWeight.w800),
                    ),
                  )),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(color: Colors.redAccent, width: 2)),
                  child: Padding(
                    padding: EdgeInsets.all(6.0),
                    child: Text(
                      'B-Left:   $budgetLeft',
                      style: GoogleFonts.lato(
                        color: Palette.PrimaryColor[100],
                        fontSize: 11,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  )),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.blueAccent, width: 2),
                      borderRadius: BorderRadius.circular(15)),
                  child: Padding(
                    padding: EdgeInsets.all(6.0),
                    child: Text(
                      'Total:   $groceryTot',
                      style: GoogleFonts.lato(
                          color: Palette.PrimaryColor[100],
                          fontSize: 11,
                          fontWeight: FontWeight.w600),
                    ),
                  )),
              label: '',
            ),
          ],
        ),
      ),
    );
  }

  Widget buldItem(
      {required Text item,
      VoidCallback? onClicked,
      required SizedBox iconBtnSzb}) {
    return Card(
      shape: RoundedRectangleBorder(
          side: BorderSide(
            color: Palette.PrimaryColor,
            width: 1.8,
          ),
          borderRadius: BorderRadius.circular(15)),
      child: ListTile(
        title: item,
        onTap: onClicked,
        trailing: iconBtnSzb,
      ),
    );
  }

  void openBudgetDialog() {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
                title: Center(
                  child: Text(
                    'Items Budget',
                    style: GoogleFonts.lato(
                        fontSize: 15, fontWeight: FontWeight.w800),
                  ),
                ),
                content: SingleChildScrollView(
                    child: TextFormField(
                        controller: budgetTextContr,
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.attach_money_rounded),
                          labelText: 'Enter Your Budget',
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                            borderSide: BorderSide(
                                color: PaletteSec.SecondaryColor, width: 2),
                          ),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: BorderSide(
                                  color: PaletteSec.SecondaryColor, width: 2)),
                          //Turning the off the hint-label when entering text
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          border: InputBorder.none,
                          labelStyle: GoogleFonts.lato(fontSize: 14),
                        ),
                        keyboardType: const TextInputType.numberWithOptions(
                            decimal: true))),
                actions: [
                  Center(
                    child: Container(
                      width: 150,
                      child: ElevatedButton(
                          style:
                              ElevatedButton.styleFrom(shape: StadiumBorder()),
                          onPressed: () {
                            setState(() {
                              if (budgetTextContr.text.isNotEmpty) {
                                budget = double.parse(budgetTextContr.text);
                                newBudget = budget;
                                if (newBudget <= 0) {
                                  newBudget = 0;
                                }
                              }
                              //Calling calculate itens total and budget left to refresh the total and budget left
                              calItemsTotal();
                              calBudgetLeft();
                            });
                            Navigator.pop(context);
                          },
                          child: Text(
                            'Save',
                            style: GoogleFonts.lato(
                                fontSize: 15, color: Colors.white),
                          )),
                    ),
                  )
                ]));
  }

  //Setting item price dialog for calculating total grocery items
  void openItemCalcutDialog(int index) {
    ItemCalTextContr.text = itemPrices[index].toString();
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
                title: Center(
                    child: Text(
                  'Item Price',
                  style: GoogleFonts.lato(
                      fontSize: 17, fontWeight: FontWeight.w800),
                )),
                content: SingleChildScrollView(
                    child: TextFormField(
                        controller: ItemCalTextContr,
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.mode_edit_outline_rounded),
                          labelText: 'Enter the Item price',
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                            borderSide: BorderSide(
                                color: PaletteSec.SecondaryColor, width: 2),
                          ),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: BorderSide(
                                  color: PaletteSec.SecondaryColor, width: 2)),
                          //Turning the off the hint-label when entering text
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          border: InputBorder.none,
                          labelStyle: GoogleFonts.lato(fontSize: 17),
                        ),
                        keyboardType: const TextInputType.numberWithOptions(
                            decimal: true))),
                actions: [
                  Container(
                    width: 120,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            shape: const StadiumBorder()),
                        onPressed: () {
                          setState(() {
                            if (ItemCalTextContr.text.isNotEmpty ||
                                !ItemCalTextContr.text.contains(',') ||
                                !ItemCalTextContr.text.contains('-')) {
                              //Adding a new item amount or updating the the existing item
                              itemPrices[index] =
                                  double.parse(ItemCalTextContr.text);
                            }
                          });
                          //Calculating the items total first before subtracing from the budget
                          calItemsTotal();
                          //Calculating the budget left
                          calBudgetLeft();
                          Navigator.pop(context);
                        },
                        child: Text('Save',
                            style: GoogleFonts.lato(
                                fontSize: 16, fontWeight: FontWeight.w500))),
                  ),
                  Container(
                    width: 50,
                    child: MaterialButton(
                      onPressed: () {
                        ItemCalTextContr.clear();
                      },
                      child: Text('C',
                          style: GoogleFonts.lato(
                              fontSize: 16, fontWeight: FontWeight.w900)),
                      color: Colors.redAccent,
                      minWidth: 50,
                    ),
                  ),
                ]));
  }

//Calculating the Items total
  void calItemsTotal() {
    groceryTot = 0.0;
    setState(() {
      for (int i = 0; i < itemPrices.length; i++) {
        groceryTot += itemPrices[i];
      }
    });
  }

//Calculating the money left with the items total from the budget
  void calBudgetLeft() {
    setState(() {
      budgetLeft = newBudget - groceryTot;
    });
  }

  //Converting ItemPrices that exceeds to millions
  String addMillionSufx(double itemPrices) {
    //1 000 000
    //10 000 000
    //100 000 000
    String itemPStr = '';
    String finalMilStr = itemPrices.toString();
    if (itemPrices > 999999) {
      itemPStr = itemPrices.toString();
      if (itemPStr.length == 7) {
        finalMilStr = '${itemPStr[0]}  M';
      } else if (itemPStr.length == 8) {
        finalMilStr = '${itemPStr[0]}${itemPStr[1]}  M';
      } else if (itemPStr.length == 9) {
        finalMilStr = '${itemPStr[0]}${itemPStr[1]}${itemPStr[2]}  M';
      }
    } else {
      return finalMilStr;
    }
    return finalMilStr;
  }
}
