import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mygrocerylist/utils/database_Helper.dart';
import 'package:mygrocerylist/views/itemsCalculatorUI.dart';
import 'package:mygrocerylist/styles/colors/pallete.dart';

class addItemsMaterialUIWidget extends StatefulWidget {
  const addItemsMaterialUIWidget(
      {super.key, required this.maxHeight, required this.groceryListName});
  final double maxHeight;
  final groceryListName;
  @override
  State<addItemsMaterialUIWidget> createState() =>
      _addItemsMaterialUIWidgetState();
}

class _addItemsMaterialUIWidgetState extends State<addItemsMaterialUIWidget> {
  final _formKey = GlobalKey<FormState>();
  List<String> items = [];
  String title = 'Items';
  TextEditingController newItemTextCront = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    items.clear();
  }

  @override
  Widget build(BuildContext context) {
    //Returning Layoutbuilder for so better experience design

    return MaterialApp(
        theme: ThemeData(
          useMaterial3: true,
          colorScheme: ColorScheme.fromSeed(
            seedColor: Palette.PrimaryColor,
            brightness: Brightness.light,
          ),
        ),
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
              backgroundColor: Palette.PrimaryColor,
              title: Text('${title.toUpperCase()}',
                  style: GoogleFonts.lato(
                      fontSize: 15,
                      fontWeight: FontWeight.w800,
                      color: Palette.PrimaryColor[100])),
              centerTitle: true,
            ),
            body: SingleChildScrollView(
              child:
                  Column(mainAxisAlignment: MainAxisAlignment.start, children: <
                      Widget>[
                Container(
                    height: widget.maxHeight,
                    child: ListView.builder(
                      itemCount: items.length,
                      itemBuilder: (BuildContext context, int index) {
                        return buldItem(
                          item: items[index],
                          iconBtnSzb: SizedBox(
                            width: 50,
                            child: Row(children: [
                              IconButton(
                                  onPressed: () {
                                    setState(() {
                                      items.removeAt(index);
                                    });
                                  },
                                  icon: Icon(
                                    Icons.delete,
                                    color: Colors.redAccent,
                                  )),
                            ]),
                          ),
                          onClicked: () {},
                        );
                      },
                    )),
                Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Form(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      key: _formKey,
                      child: Column(children: [
                        TextFormField(
                            maxLength: 25,
                            autocorrect: false,
                            enableSuggestions: false,
                            autofocus: false,
                            controller: newItemTextCront,
                            validator: (value) {
                              if (items.isEmpty) {
                                return 'Enter item description!';
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide: BorderSide(
                                    color: PaletteSec.SecondaryColor, width: 2),
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  borderSide: BorderSide(
                                      color: PaletteSec.SecondaryColor,
                                      width: 2)),
                              errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  borderSide: BorderSide(color: Colors.red)),
                              focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  borderSide: BorderSide(color: Colors.red)),
                              suffix: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      if (newItemTextCront.text != "") {
                                        items.add(newItemTextCront.text);
                                      }
                                      newItemTextCront.text = "";
                                    });
                                  },
                                  icon: Icon(Icons.add),
                                  iconSize: 35,
                                  color: Colors.black),
                              hintText: 'Enter item name',
                              hintStyle: GoogleFonts.lato(fontSize: 17),
                            )),

                        //Saving items to database
                        Container(
                          width: 200,
                          height: 45,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                shape: StadiumBorder(),
                                backgroundColor: PaletteSec.SecondaryColor),
                            onPressed: () async {
                              int itemNameid;
                              final isValidForm =
                                  _formKey.currentState!.validate();
                              if (isValidForm) {
                                itemNameid = await _addingItemsName(
                                    widget.groceryListName);

                                if (items.isNotEmpty && itemNameid != Null) {
                                  for (int i = 0; i < items.length; i++) {
                                    await _addingItems(itemNameid, items[i]);
                                  }
                                  //Using pushAndRemoveUntill to clear the routing stack to start at the first page
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => StartShoppingScreen(
                                              itemNameid: itemNameid)),
                                      (route) => route.isFirst);
                                }
                              }

                              setState(() {
                                items.clear();
                              });
                            },
                            child: Text(
                              'SAVE ITEMS',
                              style: GoogleFonts.lato(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black),
                            ),
                          ),
                        )
                      ]),
                    ))
              ]),
            )));
  }
}

Widget buldItem(
    {required String item,
    VoidCallback? onClicked,
    required SizedBox iconBtnSzb}) {
  return Card(
    shape: RoundedRectangleBorder(
        side: BorderSide(
          color: Palette.PrimaryColor,
          width: 1.8,
        ),
        borderRadius: BorderRadius.circular(15)),
    child: ListTile(
      title: Text('$item'),
      onTap: onClicked,
      trailing: iconBtnSzb,
    ),
  );
}

Future<void> _addingItems(var itemNameid, String item) async {
  await SQLHelper.createItems(item, itemNameid);
}

Future<int> _addingItemsName(String itemName) async {
  return await SQLHelper.createItemName(itemName);
}
