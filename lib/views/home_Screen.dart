import 'package:flutter/material.dart';
import 'package:mygrocerylist/views/addItemsUI.dart';
import 'package:mygrocerylist/views/itemsCalculatorUI.dart';
import 'package:mygrocerylist/utils/database_Helper.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:go_router/go_router.dart';
import 'package:mygrocerylist/styles/colors/pallete.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  final String title = 'My Lists';

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController groceryListNameTextContr = TextEditingController();
  TextEditingController updateItemNameCntr = TextEditingController();
  String groceryName = "";
  List<Map<String, dynamic>> _itemNames = [];
  bool _isLoading = true;
  void _refreshItemNamesJournal() async {
    //Loading all the item names from the database
    final data = await SQLHelper.getAllItemNames();
    setState(() {
      _itemNames = data;
      _isLoading = false;
    });
  }

//Refreshes the list names evrytime when this page is loaded
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _refreshItemNamesJournal();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(
          seedColor: Palette.PrimaryColor,
          brightness: Brightness.light,
        ),
      ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Palette.PrimaryColor,
          leading: IconButton(
            //Refreshing the journal
            onPressed: () {
              _refreshItemNamesJournal();
            },
            icon: Icon(
              Icons.refresh,
              color: Palette.PrimaryColor[100],
              size: 25,
            ),
            iconSize: 25,
          ),
          title: Text(widget.title.toUpperCase(),
              style: GoogleFonts.lato(
                  fontSize: 15,
                  fontWeight: FontWeight.w800,
                  color: Palette.PrimaryColor[100])),
          centerTitle: true,
        ),
        body: Center(
            child: _isLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                            height: 660,
                            child: ListView.builder(
                              itemCount: _itemNames.length,
                              itemBuilder: (BuildContext context, int index) {
                                return itemNameBuiler(
                                  item: Text(
                                    _itemNames[index]['title'],
                                    style: GoogleFonts.lato(
                                        fontSize: 17,
                                        fontWeight: FontWeight.w700),
                                  ),
                                  iconBtnSzb: SizedBox(
                                    width: 100,
                                    child: Row(children: [
                                      //Editing list name button
                                      IconButton(
                                        onPressed: () {
                                          int id = _itemNames[index]['id'];
                                          updateItemOpenDialog(id);
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(const SnackBar(
                                            content: Text(
                                                'List name updated successfully'),
                                          ));
                                          //Refreshing the list
                                        },
                                        icon: Icon(
                                          Icons.edit,
                                          color: PaletteSec.SecondaryColor,
                                        ),
                                      ),
                                      //deleting list name and all the items button
                                      IconButton(
                                          onPressed: () async {
                                            var id = _itemNames[index]['id'];
                                            _deleteItems(id);

                                            //Refreshing the list
                                          },
                                          icon: Icon(
                                            Icons.delete,
                                            color: Colors.red,
                                          )),
                                    ]),
                                  ),
                                  onClicked: () {
                                    //Navigating to the start shopping screen with the itemName id
                                    var itemNameId = _itemNames[index]['id'];

                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) => StartShoppingScreen(
                                                itemNameid: itemNameId)));
                                  },
                                );
                              },
                            )),
                      ],
                    ),
                  )),
        floatingActionButton: FloatingActionButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(30.0))),
          backgroundColor: PaletteSec.SecondaryColor,
          splashColor: Palette.PrimaryColor,
          onPressed: () {
            openDialog();
          },
          tooltip: 'Add Item',
          child: const Icon(
            Icons.add,
            color: Colors.black,
          ),
        ), // This trailing,
      ),
    );
  }

  void openDialog() {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Center(
                child: Text(
                  'Create New Title',
                  style: GoogleFonts.lato(
                      fontSize: 15, fontWeight: FontWeight.w900),
                ),
              ),
              content: SingleChildScrollView(
                child: Form(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    key: _formKey,
                    child: Column(
                      children: [
                        TextFormField(
                          maxLength: 17,
                          controller: groceryListNameTextContr,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Your title is empty. Enter new title!';
                            } else {
                              return null;
                            }
                          },
                          decoration: InputDecoration(
                            prefixIcon: Icon(Icons.add),
                            labelText: 'Enter new title',
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: BorderSide(
                                  color: PaletteSec.SecondaryColor, width: 2),
                            ),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide: BorderSide(
                                    color: PaletteSec.SecondaryColor,
                                    width: 2)),
                            errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide: BorderSide(color: Colors.red)),
                            focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide: BorderSide(color: Colors.red)),
                            //Turning the off the hint-label when entering text
                            floatingLabelBehavior: FloatingLabelBehavior.never,

                            labelStyle: GoogleFonts.lato(fontSize: 17),
                          ),
                        ),
                        Center(
                            child: Container(
                          width: 150,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  shape: StadiumBorder(),
                                  backgroundColor: PaletteSec.SecondaryColor),
                              onPressed: () {
                                groceryName = groceryListNameTextContr.text;
                                final isValidForm =
                                    _formKey.currentState!.validate();
                                if (isValidForm) {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => CreateGlistScreen(
                                                groceryListName: groceryName,
                                              )));
                                  groceryListNameTextContr.text = '';
                                }
                              },
                              child: Text(
                                'Save',
                                style: GoogleFonts.lato(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              )),
                        )),
                      ],
                    )),
              ),
            ));
  }

//This functin opens the dialog to edit the existing title name
  void updateItemOpenDialog(
    int id,
  ) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Center(
                  child: Text(
                'Change Title Name',
                style:
                    GoogleFonts.lato(fontSize: 15, fontWeight: FontWeight.w800),
              )),
              content: SingleChildScrollView(
                child: Form(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFormField(
                          maxLength: 17,
                          controller: updateItemNameCntr,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Your title is empty. Enter title!';
                            } else {
                              return null;
                            }
                          },
                          decoration: InputDecoration(
                            prefixIcon: Icon(Icons.mode_edit_outline_rounded),
                            labelText: 'Update title',
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: BorderSide(
                                  color: PaletteSec.SecondaryColor, width: 2),
                            ),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide: BorderSide(
                                    color: PaletteSec.SecondaryColor,
                                    width: 2)),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: BorderSide(color: Colors.red),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide: BorderSide(color: Colors.red)),
                            //Turning the off the hint-label when entering text
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                            border: InputBorder.none,
                            labelStyle: GoogleFonts.lato(fontSize: 17),
                          )),
                      Container(
                        width: 150,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                shape: StadiumBorder()),
                            onPressed: () {
                              final isValidForm =
                                  _formKey.currentState!.validate();
                              if (isValidForm) {
                                String title = updateItemNameCntr.text;
                                _updateItemName(id, title);
                                setState(
                                  () {
                                    _refreshItemNamesJournal();
                                    updateItemNameCntr.clear();
                                    Navigator.pop(context);
                                  },
                                );
                              }
                            },
                            child: Text(
                              'Save',
                              style: GoogleFonts.lato(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            )),
                      ),
                    ],
                  ),
                ),
              ),
            ));
  }

//This method builds the list items using listTiles
  Widget itemNameBuiler(
      {required Text item,
      VoidCallback? onClicked,
      required SizedBox iconBtnSzb}) {
    return Card(
      shape: RoundedRectangleBorder(
          side: BorderSide(
            color: PaletteSec.SecondaryColor,
            width: 1.8,
          ),
          borderRadius: BorderRadius.circular(15)),
      child: ListTile(
        title: item,
        onTap: onClicked,
        trailing: iconBtnSzb,
      ),
    );
  }

  Future<void> _deleteItems(var id) async {
    await SQLHelper.deleteItemName(id);
    await SQLHelper.deleteItems(id);
    _refreshItemNamesJournal();
  }

  Future<void> _updateItemName(int id, String title) async {
    await SQLHelper.updateItem(id, title);
  }
}/*
*/
